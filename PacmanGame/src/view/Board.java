/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Game;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 *
 * @author Klaudia Stpiczyńska nr. albumu 300439
 */
public class Board extends JPanel implements ActionListener {

    private Game game;
    private Timer timer;

    /**
     * Constructor that create board on the game
     */
    public Board(Game game) {
        this.game = game;
        setOpaque(true);
        setBackground(Color.BLACK);

        this.setPreferredSize(new Dimension(800, 600));

        timer = new Timer(50, this);
        timer.start();

    }

    public void setPacmanDir(int dir) {
        game.getPacman().setDir(dir);
    }

    /**
     * It paint walls in accurate place on the gameTab
     *
     * @param gameTab is a parameter of object of class Game
     */
    public void paintWalls(Graphics g, int[][] gameTab) {
        ImageIcon ibrick = new ImageIcon("resources/img/brick2.png");

        Image imageWall = ibrick.getImage();

        for (int i = 0; i < gameTab.length; ++i) {
            for (int j = 0; j < gameTab[i].length; ++j) {
                if (gameTab[i][j] == -1) {
                    g.drawImage(imageWall, j * 50, i * 50, this);
                }
            }

        }
    }

    /**
     * It paint raspberries in accurate place on the gameTab
     *
     * @param gameTab is a parameter of object of class Game
     */

    public void paintRaspberry(Graphics g, int[][] gameTab) {
        ImageIcon raspberry = new ImageIcon("resources/img/raspberry.png");

        Image imageRaspberry = raspberry.getImage();

        for (int i = 0; i < gameTab.length; ++i) {
            for (int j = 0; j < gameTab[i].length; ++j) {
                if (gameTab[i][j] == 7) {
                    g.drawImage(imageRaspberry, j * 50, i * 50, this);
                }
                
            }
            

        }

    }

    /**
     * It paint cherries in accurate place on the gameTab
     *
     * @param gameTab is a parameter of object of class Game
     */
    public void paintCherry(Graphics g, int[][] gameTab) {
        ImageIcon cherry = new ImageIcon("resources/img/cherry2.png");

        Image imageCherry = cherry.getImage();

        for (int i = 0; i < gameTab.length; ++i) {
            for (int j = 0; j < gameTab[i].length; ++j) {
                if (gameTab[i][j] == 5) {
                    g.drawImage(imageCherry, j * 50, i * 50, this);
                }
            }

        }

    }

    /**
     * It paint strawberries in accurate place on the gameTab
     *
     * @param gameTab is a parameter of object of class Game
     */
    public void paintStrawberry(Graphics g, int[][] gameTab) {
        ImageIcon strawberry = new ImageIcon("resources/img/strawberry.png");

        Image imageStrawberry = strawberry.getImage();

        for (int i = 0; i < gameTab.length; ++i) {
            for (int j = 0; j < gameTab[i].length; ++j) {
                if (gameTab[i][j] == 3) {
                    g.drawImage(imageStrawberry, j * 50, i * 50, this);
                }
            }

        }

    }

    /**
     * It paint dots in accurate place on the gameTab
     *
     * @param gameTab is a parameter of object of class Game
     */
    public void paintDots(Graphics g, int[][] gameTab) {
        ImageIcon dot = new ImageIcon("resources/img/red4.png");

        Image imageDot = dot.getImage();

        for (int i = 0; i < gameTab.length; ++i) {
            for (int j = 0; j < gameTab[i].length; ++j) {

                if (gameTab[i][j] == 0 || gameTab[i][j] == 2) {

                    g.drawImage(imageDot, j * 50, i * 50, this);
                    gameTab[i][j] = 2;
                }

            }

        }
    }

    /**
     * It paint all the components on the board (ghosts, PacMan, dots, cherries,
     * strawberries and dots)
     */
    @Override
    public void paintComponent(Graphics g) {

        if (!game.isEndOfGame()) {

            super.paintComponent(g);
            paintWalls(g, game.getGameTab());

            paintCherry(g, game.getGameTab());
            paintStrawberry(g, game.getGameTab());
            paintDots(g, game.getGameTab());
            paintRaspberry(g, game.getGameTab());

            Image image1 = game.getBlinky().getImage();
            Image image2 = game.getPinky().getImage();
            Image image3 = game.getInky().getImage();
            Image image4 = game.getClyde().getImage();

            Image pacimg = game.getPacman().getImage();

            g.drawImage(image1, game.getBlinky().getPosX(), game.getBlinky().getPosY(), this);
            g.drawImage(image2, game.getPinky().getPosX(), game.getPinky().getPosY(), this);
            g.drawImage(image3, game.getInky().getPosX(), game.getInky().getPosY(), this);
            g.drawImage(image4, game.getClyde().getPosX(), game.getClyde().getPosY(), this);

            g.drawImage(pacimg, game.getPacman().getPosX(), game.getPacman().getPosY(), this);
        }
    }

    /**
     * It repaint board if game is not over
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == timer && !game.isEndOfGame()) {

            repaint();

        }
    }

}
