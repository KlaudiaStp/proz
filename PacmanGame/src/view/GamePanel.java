/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Game;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @author Klaudia Stpiczyńska nr. albumu 300439
 */
public class GamePanel extends JPanel {

    private Game game;
    /**
     * It is label on gamePanel that shows number of lives
     */
    private JLabel numLives = new JLabel();
    /**
     * It is label on gamePanel that shows current level
     */
    private JLabel numLevel = new JLabel();
    /**
     * It is label on gamePanel that shows score
     */
    private JLabel numScore = new JLabel();
    /**
     * It is label on gamePanel that shows name of Player
     */
    private JLabel name = new JLabel();

    /**
     * Constructor that set parameteres of all labels
     */
    public GamePanel(Game game) {

        this.game = game;

        name.setText("Player: " + game.getPlayerName() + "  ");
        name.setSize(100, 32);
        name.setFont(new Font("Serif", Font.BOLD, 32));
        name.setVisible(true);
        name.setForeground(Color.RED);
        add(name);

        numLives.setText("Lives: " + game.getPacman().getLives() + "   ");
        numLives.setSize(100, 32);
        numLives.setFont(new Font("Serif", Font.BOLD, 32));
        numLives.setVisible(true);
        numLives.setForeground(Color.RED);
        add(numLives);

        numLevel.setText("Level: " + game.getLevel() + "   ");
        numLevel.setSize(100, 32);
        numLevel.setFont(new Font("Serif", Font.BOLD, 32));
        numLevel.setVisible(true);
        numLevel.setForeground(Color.RED);
        add(numLevel);

        numScore.setText("Score: " + game.getScore());
        numScore.setSize(100, 32);
        numScore.setFont(new Font("Serif", Font.BOLD, 32));
        numScore.setVisible(true);
        numScore.setForeground(Color.RED);
        add(numScore);

        setOpaque(true);
        setBackground(Color.BLACK);

        this.setPreferredSize(new Dimension(800, 200));

    }

    /**
     * It update number of lives on gamePanel
     */
    public void updateLives() {
        numLives.setText("Lives: " + game.getPacman().getLives() + "   ");
    }

    /**
     * It update level on gamePanel
     */
    public void updateLevel() {
        numLevel.setText("Level: " + game.getLevel() + "   ");
    }

    /**
     * It update score on gamePanel
     */
    public void updateScore() {
        numScore.setText("Score: " + game.getScore());
    }

}
