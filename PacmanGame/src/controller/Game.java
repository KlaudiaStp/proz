/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import view.Board;
import java.awt.FlowLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import model.BlinkyStrategy;
import model.ClydeStrategy;
import model.Ghost;
import model.InkyStrategy;
import model.Pacman;
import model.PinkyStrategy;
import model.Player;

import view.GamePanel;

/**
 *
 * @author Klaudia Stpiczyńska nr. albumu 300439
 */
public class Game extends JFrame implements KeyListener {

    /**
     * Represents part of application in which you play
     */
    Board board;
    /**
     * Is part of application when you can see name of user, score, level and
     * lives
     */

    GamePanel gamePanel;
    private Ghost blinky;
    private Ghost pinky;
    private Ghost inky;
    private Ghost clyde;
    private Pacman pacman;
    private boolean endOfGame = false;
    private int level = 1;
    private int score = 0;
    private int numOfDots = 117;
    private int numOfLives = 5;
    private String playerName;
    List<Player> players;
    Player currentPlayer;
    /**
     * Is an index of Player in list ehich has the same name as currentPlayer
     */
    int index;
    /**
     * gameTab shows where are walls and where are ghosts and PacMan at the
     * beggining of the level
     */

    private int[][] gameTab = {
        {10, 0, 0, 0, 0, -1, -1, -1, -1, -1, -1, -1, 0, 0, 0, 0},
        {0, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1, 0, -1},
        {0, -1, 0, 0, -1, 0, -1, -1, 0, 0, 0, 0, 0, -1, 0, 0},
        {0, -1, -1, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0},
        {0, -1, 0, 0, -1, 0, -1, -1, 0, 0, -1, -1, -1, -1, -1, 0},
        {0, 0, 0, 0, -1, 0, -1, 100, 100, 100, 100, -1, 0, 0, 0, 0},
        {-1, -1, -1, 0, 0, 0, -1, -1, 0, 0, -1, -1, 0, -1, 0, 0},
        {0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, -1},
        {-1, 0, 0, 0, 0, 0, -1, -1, -1, -1, 0, -1, 0, -1, -1, -1},
        {-1, 0, -1, 0, -1, 0, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0},
        {-1, -1, -1, 0, -1, 0, 0, 0, 0, -1, -1, -1, -1, 0, -1, 0},
        {0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, 0, 0, 0, -1, 0}
    };

    /**
     * Constctor which create whole game
     *
     * @param level shows from which level you start game
     */
    public Game(int level) {

        String name = JOptionPane.showInputDialog(this,
                "Insert your name",
                "Question from Pacman game",
                JOptionPane.QUESTION_MESSAGE
        );
        if (name == null) {
            name = "unknown";
        }

        name = name.trim();
        if ("".equals(name)) {
            playerName = "unknown";
        } else {
            playerName = name;
        }

        loadPlayers();
        currentPlayer = new Player(playerName, 0, 1);
        index = players.indexOf(currentPlayer);

        int m = 0;
        if (index != -1) {
            currentPlayer.setHighScore(players.get(index).getHighScore());
            currentPlayer.setHighLevel(players.get(index).getHighLevel());
            Object options[] = {"1", "3", "5", "7", players.get(index).getHighLevel()};
            m = JOptionPane.showOptionDialog(this,
                    "Your highest score is: " + players.get(index).getHighScore() + " your highest level is: "
                    + players.get(index).getHighLevel() + " From which level do you want to start?",
                    "Question from Pacman game",
                    JOptionPane.YES_NO_CANCEL_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null, options, options[4]);

        }

        this.setLayout(new FlowLayout());
//        if (m == 0) {
//            level = 1;
//        } else if (m == 1) {
//            level = players.get(index).getHighLevel();
//        }

        switch (m) {
            case 0:
                level = 1;
                break;
            case 1:
                level = 3;
                break;
            case 2:
                level = 5;
                break;
            case 3:
                level = 7;
                break;
            case 4:
                level = players.get(index).getHighLevel();
                break;
        }

        this.level = level;

        setUpLevel();

        setSize(800, 800);
        this.setResizable(false);
        setTitle("PacMan");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        this.addKeyListener(this);
        startAll();

    }

    public String getPlayerName() {
        return playerName;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    /**
     * override method which set PacMan direction depending on pressed key on
     * keyboard
     *
     * @param e represents key on keyboard
     */
    @Override
    public void keyPressed(KeyEvent e) {
        // System.out.println("key "+e.getKeyCode());
        int keyCode = e.getKeyCode();
        switch (keyCode) {
            case KeyEvent.VK_UP:
                board.setPacmanDir(0);
                break;
            case KeyEvent.VK_DOWN:
                board.setPacmanDir(180);
                break;
            case KeyEvent.VK_LEFT:
                board.setPacmanDir(270);
                break;
            case KeyEvent.VK_RIGHT:
                board.setPacmanDir(90);
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        // System.out.println("released");
    }

    public GamePanel getGamePanel() {
        return gamePanel;
    }

    public Ghost getBlinky() {
        return blinky;
    }

    public Ghost getPinky() {
        return pinky;
    }

    public Ghost getInky() {
        return inky;
    }

    public Ghost getClyde() {
        return clyde;
    }

    public Pacman getPacman() {
        return pacman;
    }

    /**
     * This method support case when PacMan and one of ghosts are in collision,
     * when PacMan is inmortal (because it ate cherry) ghost return to start
     * position, if it is not inmortal PacMan lose one life, return to start
     * position and gamePanel is updated also if it was last one it invoke
     * gameOver() method
     *
     * @param ghost
     */
    public void collision(Ghost ghost) {
        if (pacman.isInmortal()) {
            ghost.returnToStart();
        } else if (pacman.getLives() == 1) {
            pacman.setLives(0);
            numOfLives = 0;
            gamePanel.updateLives();
            gameOver();
        } else {
            pacman.decreaseLives();
            --numOfLives;
            pacman.setPosX(0);
            pacman.setPosY(0);
            pacman.setDir(90);
            blinky.returnToStart();
            pinky.returnToStart();
            inky.returnToStart();
            clyde.returnToStart();
            gamePanel.updateLives();
        }

    }

    /**
     * This method save the score if it is the highest score of player, it also
     * ask the user is he or she want to play again
     */
    public void gameOver() {
        endOfGame = true;
        if (currentPlayer.getHighScore() < score) {
            currentPlayer.setHighScore(score);
        }
        if (currentPlayer.getHighLevel() < level) {
            currentPlayer.setHighLevel(level);
        }
        int tmpScore = score;
        int tmpLevel = level;
        score = 0;
        numOfLives = 5;
        pacman.setLives(5);
        level = 1;
        setUpLevel();

        Object options[] = {"Yes!", "No"};
        int n = JOptionPane.showOptionDialog(this,
                "Game Over. Your result:  " + tmpScore + " at level: " + tmpLevel + "\nDo you want to play again?",
                "Question from Pacman game",
                JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null, options, options[1]);

        if (n == 0) {

            endOfGame = false;
            startAll();

        } else if (n == 1) {
            index = players.indexOf(currentPlayer);
            if (index != -1) {
                players.set(index, currentPlayer);
            } else {
                players.add(currentPlayer);
            }
            savePlayers();
            System.exit(0);

        }

    }

    public boolean isEndOfGame() {
        return endOfGame;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getNumOfDots() {
        return numOfDots;
    }

    public void setNumOfDots(int numOfDots) {
        this.numOfDots = numOfDots;
    }

    public int getNumOfLives() {
        return numOfLives;
    }

    public void setNumOfLives(int numOfLives) {
        this.numOfLives = numOfLives;
    }

    public int getLevel() {
        return level;
    }

    /**
     * This method every level, it remove board and gamePanel, stops all the
     * ghosts and PacMan, then it creates new elements and it randomly choose
     * places where instead of dots are strawberries and cherries on board
     */
    public void setUpLevel() {

        if (board != null) {
            this.remove(board);
        }
        if (gamePanel != null) {
            this.remove(gamePanel);
        }

        numOfDots = 117;

        if (pacman != null) {
            pacman.stopPacman();
            pacman = null;
        }
        if (blinky != null) {
            blinky.stopGhost();
            blinky = null;
        }
        if (inky != null) {
            inky.stopGhost();
            inky = null;
        }
        if (pinky != null) {
            pinky.stopGhost();
            pinky = null;
        }
        if (clyde != null) {
            clyde.stopGhost();
            clyde = null;
        }
        board = new Board(this);

        blinky = new Ghost(this, "gh1", 7 * 50, 5 * 50, 250 - 10 * (level / 5), new BlinkyStrategy(this));
        pinky = new Ghost(this, "gh2", 8 * 50, 5 * 50, 125 - 10 * (level / 5), new PinkyStrategy(this));
        inky = new Ghost(this, "gh3", 9 * 50, 5 * 50, 150 - 10 * (level / 5), new InkyStrategy(this));
        clyde = new Ghost(this, "gh4", 10 * 50, 5 * 50, 500 - 10 * (level / 5), new ClydeStrategy(this));

        pacman = new Pacman(this, 0, 0, 100);

        pacman.setLives(numOfLives);

        this.add(board);

        gamePanel = new GamePanel(this);

        this.add(gamePanel);
        gamePanel.updateLives();
        gamePanel.updateLevel();
        gamePanel.updateScore();

        changeTab();

        for (int i = 0; i < gameTab.length; ++i) {
            for (int j = 0; j < gameTab[i].length; ++j) {
                double rand = Math.random();
                if (rand < (1.0 / level * 0.05) && gameTab[i][j] == 0) {
                    gameTab[i][j] = 3;
                }
            }

        }

        for (int i = 0; i < gameTab.length; ++i) {
            for (int j = 0; j < gameTab[i].length; ++j) {
                double rand2 = Math.random();
                if (rand2 < (1.0 / level * 0.05) && gameTab[i][j] == 0) {
                    gameTab[i][j] = 5;
                }
            }

        }

        for (int i = 0; i < gameTab.length; ++i) {
            for (int j = 0; j < gameTab[i].length; ++j) {
                double rand3 = Math.random();
                if (rand3 < (1.0 / level * 0.05) && gameTab[i][j] == 0) {
                    gameTab[i][j] = 7;
                }
            }

        }

    }

    public int[][] getGameTab() {
        return gameTab;
    }

    /**
     * It checks if Pacman can Move to a place with coordinates like x and y
     *
     * @param x represent coordinate x of a point on board
     * @param y represent coordinate x of a point on board
     * @return can character move on this place
     */
    public boolean canMovePacman(int x, int y) {
        return pacman.isWalkThroughWalls() ? true : gameTab[y / 50][x / 50] != -1;
    }

    /**
     * It checks if character can Move to a place with coordinates like x and y
     *
     * @param x represent coordinate x of a point on board
     * @param y represent coordinate x of a point on board
     * @return can character move on this place
     */
    public boolean canMove(int x, int y) {
        return gameTab[y / 50][x / 50] != -1;
    }

    /**
     * Check if there is there a dot on point with cordinates x and y
     *
     * @param x represent coordinate x of a point on board
     * @param y represent coordinate x of a point on board
     * @return is there a dot
     */
    public boolean isADot(int x, int y) {
        return gameTab[x][y] == 2;
    }

    /**
     * Check if there is there a strawberry on point with cordinates x and y
     *
     * @param x represent coordinate x of a point on board
     * @param y represent coordinate x of a point on board
     * @return is there a strawberry
     */
    public boolean isStrawberry(int x, int y) {
        return gameTab[x][y] == 3;
    }

    /**
     * Check if there is there a cherry on point with cordinates x and y
     *
     * @param x represent coordinate x of a point on board
     * @param y represent coordinate x of a point on board
     * @return is there a cherry
     */
    public boolean isCherry(int x, int y) {
        return gameTab[x][y] == 5;
    }

    /**
     * Check if there is there a raspberry on point with cordinates x and y
     *
     * @param x represent coordinate x of a point on board
     * @param y represent coordinate x of a point on board
     * @return is there a raspberry
     */
    public boolean isRaspberry(int x, int y) {
        return gameTab[x][y] == 7;
    }

    /**
     * Its assign value to game[i][j] *
     */
    public void updateGameTab(int i, int j, int value) {
        gameTab[i][j] = value;
    }

    /**
     * If there are no dots on board it increase value of level and set up new
     * level
     */
    public void endOfLevel() {

        if (numOfDots == 0) {
            endOfGame = true;
            ++level;
            setUpLevel();
            endOfGame = false;
            startAll();
        }
    }

    /**
     * If in gameTab is value -2, 3, 5 or 2 it changes it to 0
     */
    public void changeTab() {
        for (int i = 0; i < gameTab.length; ++i) {
            for (int j = 0; j < gameTab[i].length; ++j) {
                if (gameTab[i][j] == -2 || gameTab[i][j] == 3 || gameTab[i][j] == 5 || gameTab[i][j] == 2 || gameTab[i][j] == 7) {
                    gameTab[i][j] = 0;
                }
            }

        }
    }

    /**
     * It loads data of players from file
     */
    public void loadPlayers() {
        BufferedReader in = null;
        players = new ArrayList<>();
        try {

            in = new BufferedReader(new FileReader("players.txt"));
            String name = in.readLine();
            while (!(name == null)) {

                int score = Integer.parseInt(in.readLine());
                int level = Integer.parseInt(in.readLine());
                players.add(new Player(name, score, level));
                name = in.readLine();
            }
            in.close();

        } catch (Exception e) {
            File file = new File("players.txt");

        }
    }

    /**
     * It save data of players to the file
     */
    public void savePlayers() {
        PrintWriter out = null;
        try {
            out = new PrintWriter("players.txt");

            for (Player player : players) {
                out.println(player.getName());
                out.println(player.getHighScore());
                out.println(player.getHighLevel());

            }
            out.close();

        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * It starts the movement of all ghosts and PacMan
     */
    public void startAll() {
        blinky.startGhost();
        inky.startGhost();
        pinky.startGhost();
        clyde.startGhost();
        pacman.startPacman();
    }

    public Board getBoard() {
        return board;
    }

}
