/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Objects;

/**
 *
 * @author Klaudia Stpiczyńska nr. albumu 300439
 */
public class Player {

    private String name;
    private int highScore;
    private int highLevel;

    public Player(String name, int highScore, int highLevel) {
        this.name = name;
        this.highScore = highScore;
        this.highLevel = highLevel;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        if(name!= null){
            hash=3*hash + name.hashCode();
        }
        return hash;
    }

    /**
     *
     * @return is two players have the same name
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Player other = (Player) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    public String getName() {
        return name;
    }

    public int getHighScore() {
        return highScore;
    }

    public void setHighScore(int highScore) {
        this.highScore = highScore;
    }

    public int getHighLevel() {
        return highLevel;
    }

    public void setHighLevel(int highLevel) {
        this.highLevel = highLevel;
    }

}
