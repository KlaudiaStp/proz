/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Game;

/**
 *
 * @author Klaudia Stpiczyńska 
 * nr. albumu 300439
 */
public interface GhostStrategy {
    
    public GhostDir findMove(Game game);
        
    
    
}
