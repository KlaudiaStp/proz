/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Game;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.Timer;

/**
 *
 * @author Klaudia Stpiczyńska nr. albumu 300439
 */
public class Pacman implements ActionListener {

    private Image image;

    private Image image000;
    private Image image090;
    private Image image180;
    private Image image270;

    private int posX;
    private int posY;

    private int step = 0;

    private int dx = 0;
    private int dy = 0;

    private int ndx = 0;
    private int ndy = 0;

    int dir;

    private Timer timer;

    private int speed;
    private int nextX;
    private int nextY;

    private int lives = 5;
    /**
     * Inform is PacMan inmortal
     */
    private boolean inmortal = false;
    private int countInmortal;
    private final static int timeInmortal = 5000;

    private boolean walkThroughWalls = false;
    private int countWalk;
    private final static int timeWalk = 7000;


    Game game;

    /**
     * Constructor which set name, position, image and speed of PacMan
     */
    public Pacman(Game game, int px, int py, int speed) {
        this.game = game;
        posX = px;
        posY = py;
        this.speed = speed;
        setDir(90);
        ImageIcon ii = new ImageIcon("resources/img/pacman000.png");
        image000 = ii.getImage().getScaledInstance(50, 50, 0);
        ii = new ImageIcon("resources/img/pacman090.png");
        image090 = ii.getImage().getScaledInstance(50, 50, 0);
        ii = new ImageIcon("resources/img/pacman180.png");
        image180 = ii.getImage().getScaledInstance(50, 50, 0);
        ii = new ImageIcon("resources/img/pacman270.png");
        image270 = ii.getImage().getScaledInstance(50, 50, 0);

        image = image090;
        timer = new Timer(speed, this);

    }

    /**
     * It count next position of PacMan depending on direction of PacMan, if it
     * can move it also check can PacMan eat dot, cherry or strawberry
     *
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        boolean move = true;

        if (inmortal) {
            --countInmortal;
            if (countInmortal == 0) {
                inmortal = false;

            }
        }

        if (walkThroughWalls) {
            --countWalk;
            if (countWalk == 0) {
                walkThroughWalls = false;

            }
        }

        if (step == 0) {

            setPacmanImg();
            dx = ndx;
            dy = ndy;
            if (!((posX + 5 * dx < 800) && (posY + 5 * dy < 600)
                    && (posX + 5 * dx >= 0) && (posY + 5 * dy >= 0)) || !game.canMovePacman(posX + 5 * dx, posY + 5 * dy)) {
                dx = 0;
                dy = 0;
                move = false;

            }

        }

        if (move) {
            posX += dx;
            posY += dy;
            step = (step + 1) % 5;
            if (step == 0) {
                eatDot(posY / 50, posX / 50);
                eatStrawberry(posY / 50, posX / 50);
                eatCherry(posY / 50, posX / 50);
                eatRaspberry(posY / 50, posX / 50);
            }

        }

    }

    public Rectangle getRectangle() {
        return new Rectangle(posX, posY, 50, 50);
    }

    /**
     * It setting direction of PacMan and depending on direction it is setting
     * ndx and ndy
     */
    public void setDir(int dir) {
        this.dir = dir;
        switch (dir) {
            case 0:
                ndx = 0;
                ndy = -10;
                break;
            case 90:
                ndx = 10;
                ndy = 0;
                break;
            case 180:
                ndx = 0;
                ndy = 10;
                break;
            case 270:
                ndx = -10;
                ndy = 0;
                break;
        }
    }

    /**
     * It counting and saving next position of PacMan depending on its position
     * and direction
     */
    public void countNextPosition() {
        switch (dir) {
            case 0:
                nextY = posY - 50;
                break;
            case 90:
                nextX = posX + 50;
                break;
            case 180:
                nextY = posY + 50;
                break;
            case 270:
                nextX = posX - 50;
                break;

        }
    }

    private void setPacmanImg() {
        switch (dir) {
            case 0:
                image = image000;
                break;
            case 90:
                image = image090;
                break;
            case 180:
                image = image180;
                break;
            case 270:
                image = image270;
                break;
        }
    }

    public Image getImage() {
        return image;
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public int getLives() {
        return lives;
    }

    public void setLives(int lives) {
        this.lives = lives;
    }

    public boolean isInmortal() {
        return inmortal;
    }

    /**
     * It decrease lives of PacMan
     */
    public synchronized void decreaseLives() {
        if (lives > 0) {
            --lives;
            step = 0;
        }
    }

    /**
     * When PacMan is in collision with dot its remove dot from board and update
     * score It checks if it was the last dot on board
     *
     * @param i coordinate x on board
     * @param j coordinate y on board
     */
    public void eatDot(int i, int j) {
        if (game.isADot(i, j)) {
            game.setScore(game.getScore() + 100 + 25 * (game.getLevel() / 3));
            game.updateGameTab(i, j, -2);
            game.getGamePanel().updateScore();
            game.setNumOfDots(game.getNumOfDots() - 1);
            game.endOfLevel();
        }

    }

    /**
     * When PacMan is in collision with strawberry its remove strawberry from
     * board, update score and lives It checks if it was the last dot or
     * strawberry on board
     *
     * @param i coordinate x on board
     * @param j coordinate y on board
     */
    public void eatStrawberry(int i, int j) {
        if (game.isStrawberry(i, j)) {
            game.setScore(game.getScore() + 100 + 25 * (game.getLevel() / 3));
            game.updateGameTab(i, j, -2);
            game.getGamePanel().updateScore();
            game.setNumOfDots(game.getNumOfDots() - 1);
            ++lives;
            game.setNumOfLives(game.getNumOfLives() + 1);
            game.getGamePanel().updateLives();
            game.endOfLevel();
        }
    }

    /**
     * When PacMan is in collision with cherry its remove cherry from board,
     * update score and make PacMan inmortal for 5 second It checks if it was
     * the last dot or cherry on board
     *
     * @param i coordinate x on board
     * @param j coordinate y on board
     */
    public void eatCherry(int i, int j) {
        if (game.isCherry(i, j)) {
            game.setScore(game.getScore() + 100 + 25 * (game.getLevel() / 3));
            game.updateGameTab(i, j, -2);
            game.getGamePanel().updateScore();
            game.setNumOfDots(game.getNumOfDots() - 1);
            game.endOfLevel();
            game.getBlinky().setGrey(true);
            game.getClyde().setGrey(true);
            game.getInky().setGrey(true);
            game.getPinky().setGrey(true);
            inmortal = true;

            countInmortal = timeInmortal / speed;

        }
    }

    /**
     * When PacMan is in collision with raspberry its remove raspberry from board,
     * update score and make PacMan inmortal for 5 second It checks if it was
     * the last dot or cherry on board
     *
     * @param i coordinate x on board
     * @param j coordinate y on board
     */
    public void eatRaspberry(int i, int j) {
        if (game.isRaspberry(i, j)) {
            game.setScore(game.getScore() + 100 + 25 * (game.getLevel() / 3));
            game.updateGameTab(i, j, -2);
            game.getGamePanel().updateScore();
            game.setNumOfDots(game.getNumOfDots() - 1);
            game.endOfLevel();
            walkThroughWalls = true;
            countWalk = timeWalk / speed;
        }

    }

    /**
     * It stops timer of PacMan
     */
    public void stopPacman() {
        timer.stop();
    }

    /**
     * It move PacMan to start position and start timer of it
     */
    public void startPacman() {
        step = 0;
        posX = 0;
        posY = 0;
        timer.start();
    }

    public int getNextX() {
        return nextX;
    }

    public int getNextY() {
        return nextY;
    }

    public boolean isWalkThroughWalls() {
        return walkThroughWalls;
    }

    public int getDx() {
        return dx;
    }

    public int getDy() {
        return dy;
    }


}
