/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Game;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.Timer;

/**
 *
 * @author Klaudia Stpiczyńska nr. albumu 300439
 */
public class Ghost implements ActionListener {

    private Image image;
    private Image image2;
    private int posX;
    private int posY;
    private int startX;
    private int startY;

    private int step = 0;

    private int dx = 0;
    private int dy = 0;

    private Timer timer;

    private int speed;

    private String name;
    /**
     * It informs is ghosts grey
     */
    private boolean grey = false;
    /**
     * It counts beats of clock when ghost is grey
     */
    private int countGrey = 0;
    private final static int timeOfGrey = 5000;
    private GhostStrategy myStrategy;

    Game game;

    /**
     * Constructor which set name, position, speed, image and strategy of ghost
     */
    public Ghost(Game game, String name, int px, int py, int speed, GhostStrategy strat) {
        this.game = game;
        this.name = name;
        myStrategy = strat;
        posX = px;
        posY = py;
        startX = px;
        startY = py;
        this.speed = speed;
        ImageIcon ii = new ImageIcon("resources/img/" + name + ".png");
        image = ii.getImage().getScaledInstance(50, 50, 0);
        ImageIcon ig = new ImageIcon("resources/img/gh0.png");
        image2 = ig.getImage().getScaledInstance(50, 50, 0);
        timer = new Timer(speed, this);
        //timer.start();
    }

    public Rectangle getRectangle() {
        return new Rectangle(posX, posY, 50, 50);
    }

    public Image getImage() {

        return grey ? image2 : image;
    }

    /**
     * It describe how ghost move and check if there is collision beetween
     * PacMan and ghost
     */

    @Override
    public void actionPerformed(ActionEvent e) {

        boolean move = true;

        if (grey) {
            --countGrey;
            if (countGrey == 0) {
                grey = false;

            }
        }

        if (step == 0) {

            GhostDir dir = myStrategy.findMove(game);
            dx = dir.getDx();
            dy = dir.getDy();

            if (!((posX + 5 * dx < 800) && (posY + 5 * dy < 600)
                    && (posX + 5 * dx >= 0) && (posY + 5 * dy >= 0)) || !game.canMove(posX + 5 * dx, posY + 5 * dy)) {
                dx = 0;
                dy = 0;
                move = false;
            }

        }

        if (move) {
            posX += dx;
            posY += dy;
            step = (step + 1) % 5;

        }
        if (this.getRectangle().intersects(game.getPacman().getRectangle())) {
            game.collision(this);

        }

    }

    public int getPosX() {
        return posX;
    }


    public int getPosY() {
        return posY;
    }


    public boolean isGrey() {
        return grey;
    }

    /**
     * It set if ghost is grey and set countGrey
     */
    public void setGrey(boolean grey) {
        this.grey = grey;
        if (grey) {
            countGrey = timeOfGrey / speed;

        }
    }

    public int getStartX() {
        return startX;
    }

    public int getStartY() {
        return startY;
    }

    /**
     * It sets ghost position to start position
     */
    public void returnToStart() {
        posX = startX;
        posY = startY;
        step = 0;
    }

    /**
     * It stops timer of the ghost
     */
    public void stopGhost() {
        timer.stop();
    }

    /**
     * It move ghost to start position and start timer of ghosts
     */
    public void startGhost() {
        returnToStart();
        step = 0;
        timer.start();
    }


}
