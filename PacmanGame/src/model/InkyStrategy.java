/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Game;

/**
 *
 * @author Klaudia Stpiczyńska nr. albumu 300439
 */
public class InkyStrategy implements GhostStrategy {

    private Game game;
    private GhostStrategy ghostStrat;
    private int count = 0;
    private GhostStrategy[] strat = new GhostStrategy[3];
    private int stratNumber = 0;

    public InkyStrategy(Game game) {
        this.game = game;
        strat[0] = new BlinkyStrategy(game);
        strat[1] = new PinkyStrategy(game);
        strat[2] = new ClydeStrategy(game);
        ghostStrat = strat[0];
        stratNumber = 0;

    }

    /**
     * It find move according to one of the strategy (Blinky, Pinky or Clyde),
     * the strategy is changing during the game (it also provides that Inky can
     * not move to the wall)
     *
     * @return direction in which ghost should move
     */
    @Override
    public GhostDir findMove(Game game) {

        if (count == 0) {
            stratNumber = (stratNumber + 1) % 3;
            ghostStrat = strat[stratNumber];
            count = 1;
            return ghostStrat.findMove(game);
        } else {
            count = (count + 1) % 50;
            return ghostStrat.findMove(game);
        }
    }

}
