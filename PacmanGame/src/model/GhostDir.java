/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Game;

/**
 *
 * @author Klaudia Stpiczyńska 
 * nr. albumu 300439
 */
public class GhostDir {
    private int dx;
    private int dy;
    private int distance;

    public int getDx() {
        return dx;
    }

    public int getDy() {
        return dy;
    }

    public int getDistance() {
        return distance;
    }

    public GhostDir(int dx, int dy) {
        this.dx = dx;
        this.dy = dy;
    }
    
    /**
     * If ghost can not move to desire position distance is equal 1000000000, if it can 
     * distance is equal distance beetween PacMan and future ghosts position
     * @param px coordinate x of PacMan position
     * @param py coordinate y of PacMan position
     * @param gx coordinate x of ghost position
     * @param gy coordinate y of ghost position
     */
    
    public void countDistance(int px, int py, int gx, int gy, Game game){
        if(!((gx + 5 * dx < 800) && (gy + 5 * dy < 600)
                    && (gx+ 5 * dx >= 0) && (gy + 5 * dy >= 0)) || !game.canMove(gx+dx*5, gy+dy*5)){
            distance=1000000000;
        }
        else{
            distance=Math.abs(px-(gx+dx*5))+Math.abs(py-(gy+dy*5));
        }
    }
}
