/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Game;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Klaudia Stpiczyńska nr. albumu 300439
 */
public class PinkyStrategy implements GhostStrategy {

    private Game game;

    public PinkyStrategy(Game game) {
        this.game = game;
    }

    /**
     * It find move when the distance beetween PacMan next position and Pinky is
     * the smallest (it also provides that Pinky can not move to the wall)
     *
     * @return direction in which ghost should move
     */
    @Override
    public GhostDir findMove(Game game) {
        game.getPacman().countNextPosition();
        int px = game.getPacman().getNextX();
        int py = game.getPacman().getNextY();
        int gx = game.getPinky().getPosX();
        int gy = game.getPinky().getPosY();

        GhostDir moveNorth = new GhostDir(0, -10);
        GhostDir moveEast = new GhostDir(10, 0);
        GhostDir moveSouth = new GhostDir(0, 10);
        GhostDir moveWest = new GhostDir(-10, 0);

        moveNorth.countDistance(px, py, gx, gy, game);
        moveEast.countDistance(px, py, gx, gy, game);
        moveSouth.countDistance(px, py, gx, gy, game);
        moveWest.countDistance(px, py, gx, gy, game);

        List<GhostDir> dirs = new ArrayList<>();
        dirs.add(moveNorth);
        dirs.add(moveEast);
        dirs.add(moveSouth);
        dirs.add(moveWest);
        Collections.sort(dirs, (e1, e2) -> e1.getDistance() - e2.getDistance());

        return dirs.get(0);
    }

}
