/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;
import controller.Game;

/**
 *
 * @author klaud
 */
public class Test {
    
    public static void main (String[] args ){
        PacmanTest pacmanTest = new PacmanTest();
        pacmanTest.createGame();
        pacmanTest.numberOfLives();
        pacmanTest.boardCreated();
        pacmanTest.gamePanelCreated();
        pacmanTest.numOfDots();
        pacmanTest.isLevelCorrect();
        pacmanTest.isGameOverCorrect();
    } 
}
