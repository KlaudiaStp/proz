/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;

import model.Pacman;
import controller.Game;
import model.BlinkyStrategy;
import model.ClydeStrategy;
import model.Ghost;
import model.InkyStrategy;
import model.PinkyStrategy;
import view.Board;
import view.GamePanel;

/**
 *
 * @author klaud
 */
public class PacmanTest {

    Game game = new Game(1);

    public void createGame() {
        game.setUpLevel();
        game.setVisible(true);
        game.startAll();
    }

    public void numberOfLives() {
        Pacman pacman = new Pacman(game, 0, 0, 100);
        assert pacman.getLives() == 5 : "Pacman number of lives is incorrect";

    }

    public void boardCreated() {
        assert game.getBoard() != null : "Board was not created";
    }

    public void gamePanelCreated() {
        assert game.getGamePanel() != null : "Game panel was not created";
    }

    public void numOfDots() {
        assert game.getNumOfDots()==117 : "Number od dots is incorrect";
    }
    public void isLevelCorrect(){
        game.setNumOfDots(0);
        game.endOfLevel();
        assert game.getLevel()==2 : "Levels are not changing correctly";
    } 
    
    public void isGameOverCorrect(){
        game.setNumOfLives(0);
        game.gameOver();
        assert game.isEndOfGame()==true :"Game is not closing correctly";
    }
}
