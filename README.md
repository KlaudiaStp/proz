# PROZ

Dokumentacja: PacmanGame/documentation/javadoc/index.html

Zbudowanie i uruchomienie programu:
(Trzeba mieć w ścieżce ustawione pliki wykonywalne jdk)

System Linux:
1)Wejść do katalogu PacmanGame/exe
2)Skompilować pliki i utworzyć archiwum jar poleceniem: sh pacbuild.sh
3)Uruchomić program poleceniem: sh pacrun.sh

System Windows:
1)Wejść do katalogu PacmanGame/exe
2)Skompilować pliki i utworzyć archiwum jar poleceniem: pacbuild.bat
3)Uruchomić program poleceniem: start pacrun.ba

Jak działa aplikacja:

Aplikacja służy do gry w PacMana. 
Celem gry jest "zjedzenie" jak największej liczby kropek (za każdą dostaje się punkty).
PO zjedzeniu wszystkich kropek na planszy, przechodzi się do nastpnego poziomu.
Jeśli PacMan wejdzie w kolizję z którymś z duchów, traci życie.
Wyjątkiem jest sytuacja, kiedy PacMan zje wiśnię, wtedy przez 5 sekund duchy są szare i to PacMan może je "zjeść".
Zjedzenie truskawki daje PacManowi jedno dodatkowe życie.
Zapisuje najwyższy wynik gracza.
Można zacząc grę o najwyższego poziomu na jakim się skończyło.
Wraz z poziomami malej liczba truskawek i wsienek na planszy oraz rośnie prędkość duchów.
Wraz z poziomami liczba punktów za kropkę rośnie.

Zmiany:
Zbudowanie i uruchomienie testów:
(Trzeba mieć w ścieżce ustawione pliki wykonywalne jdk)

System Linux:
1)Wejść do katalogu PacmanGame/exe
2)Skompilować pliki i utworzyć archiwum jar poleceniem: sh testbuild.sh
3)Uruchomić program poleceniem: sh testrun.sh

System Windows:
1)Wejść do katalogu PacmanGame/exe
2)Skompilować pliki i utworzyć archiwum jar poleceniem: testbuild.bat
3)Uruchomić program poleceniem: start testrun.bat

Do programu:
Jeśli PacMan zje malinę przez 7 sekund może chodzić po ścianach.
Jeśli PacMan straci życie duchy wracają na swoją pozycję początkową.
Przy kolejnej grze danego użytkownika (kiedy można wybrać level) dodałam możliwość zaczęcia od 3, 5 i 7 poziomu 
(żeby móc sprawdzić trudność leveli i zaczęcie od najwyższego poziomu na jakim się skończyło).
Ostatni przycisk w tym oknie pozwala na zaczęcie gry od najwyższego poziomu na jakim się skończyło.



